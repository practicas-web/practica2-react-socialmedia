
import './App.css'
import SocialMediaCardGreen from './components/SocialMediaCardGreen'
import SocialMediaCardRed from './components/SocialMediaCardRed'
import TitleSocialMedia from './components/TitleSocialMedia'

import OverviewCardGreen from './components/OverviewCardGreen'
import OverviewCardRed from './components/OverviewCardRed'
import TitleOverview from './components/TitleOverview'

import facebook from './public/img/icon-facebook.svg'
import instagram from './public/img/icon-instagram.svg'
import twitter from './public/img/icon-twitter.svg'
import youTube from './public/img/icon-youtube.svg'

function App() {
  return (
    <div className='App'>

      <div className="container-title-card-social">
        <TitleSocialMedia/>
      </div>
       
      <div className='container-card-social'>
        <SocialMediaCardGreen imagePath={facebook} nameCard='@nathanf' classBorder='border-facebook' numberCard='1987' numberUp='12 Today' />
        <SocialMediaCardGreen imagePath={twitter} nameCard='@nathanf' classBorder='border-twitter' numberCard='1044' numberUp='99 Today' />
        <SocialMediaCardGreen imagePath={instagram} nameCard='@realnathanf' classBorder='border-instagram' numberCard='11k' numberUp='1099 Today' />
        <SocialMediaCardRed imagePath={youTube} nameCard='Nathan F.' classBorder='border-youtube' numberCard='8239' numberUp='144 Today' />
      </div>

      <div className="container-title-card-over">
        <TitleOverview/>
      </div>

      <div className='container-card-overview'>
        <OverviewCardGreen nameCard='Page Views' imagePath={facebook} numberCard='87' numberUp='3%'/>
        <OverviewCardRed imagePath={facebook} nameCard='Likes' numberCard='52' numberUp='2%'/>
        <OverviewCardGreen imagePath={instagram} nameCard='Likes' numberCard='5462' numberUp='2257%'/>
        <OverviewCardGreen imagePath={instagram} nameCard='Profile Views' numberCard='52k' numberUp='1375%'/>
      </div>

      <div className='container-card-overview'>
        <OverviewCardGreen imagePath={twitter} nameCard='Retweets' numberCard='117' numberUp='303%'/>
        <OverviewCardGreen imagePath={twitter} nameCard='Likes' numberCard='507' numberUp='553%'/>
        <OverviewCardRed imagePath={youTube} nameCard='Likes' numberCard='107' numberUp='19%'/>
        <OverviewCardRed imagePath={youTube} nameCard='Total Views' numberCard='1407' numberUp='12%'/>
      </div>

      


    </div>
  )
}

export default App
