import './SocialMedia.css'
import arrowUp from '../public/img/icon-up.svg'

function SocialMediaCardGreen(props) {
  return (
    <div className={`cards-social ${props.classBorder}`}>
      <div className='image-text'>
        <div>
          <img src={props.imagePath} />
        </div>
        <h2>{props.nameCard}</h2>
      </div>
      <p>{props.numberCard}</p>
      <h3> FOLLOWERS</h3>
      <div className='image-followers'>
        <div>
          <img src={arrowUp} />
        </div>
        <h4 className='green-text-estatus'>{props.numberUp}</h4>
      </div>
    </div>
  )
}

export default SocialMediaCardGreen
