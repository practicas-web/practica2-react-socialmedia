import './SocialMedia.css'


function TitleSocialMedia(props) {
  return (
    <div className="container-title">

      <div className="text-superior">
        <h1 className>Social Media Dashboard</h1>
        <p className>Total Followers: 23,004</p>
       </div>

    </div>
  )
}

export default TitleSocialMedia