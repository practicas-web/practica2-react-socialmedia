import './OverviewCard.css'
import arrowUp from '../public/img/icon-up.svg'

function OverviewCardGreen(props) {
  return (
    <div className={`over-cards ${props.classBorder}`}>
      <div className='image-description'>
        <h2>{props.nameCard}</h2>
        <div>
          <img src={props.imagePath} />
        </div>
      </div>
      <p>{props.numberCard}</p>
      <div className='image-icon'>
        <div>
          <img src={arrowUp} />
        </div>
        <h4 className='green-icon'>{props.numberUp}</h4>
      </div>
    </div>
  )
}

export default OverviewCardGreen
