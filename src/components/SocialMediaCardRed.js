import './SocialMedia.css'
import arrowDown from '../public/img/icon-down.svg'

function SocialMediaCardRed(props) {
  return (
    <div className={`cards-social ${props.classBorder}`}>
      <div className='image-text'>
        <div>
          <img src={props.imagePath} />
        </div>
        <h2>{props.nameCard}</h2>
      </div>
      <p>{props.numberCard}</p>
      <h3>SUBSCRIBERS</h3>
      <div className='image-followers'>
        <div>
          <img src={arrowDown} />
        </div>
        <h4 className='red-text-estatus'>{props.numberUp}</h4>
      </div>
    </div>
  )
}

export default SocialMediaCardRed
