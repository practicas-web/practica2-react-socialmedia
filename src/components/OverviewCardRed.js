import './OverviewCard.css'
import arrowDown from '../public/img/icon-down.svg'

function OverviewCardRed(props) {
  return (
    <div className={`over-cards ${props.classBorder}`}>
      <div className='image-description'>
      <h2>{props.nameCard}</h2>
        <div>
          <img src={props.imagePath} />
        </div>
        
      </div>
      <p>{props.numberCard}</p>
     
      <div className='image-icon'>
        <div>
          <img src={arrowDown} />
        </div>
        <h4 className='red-icon'>{props.numberUp}</h4>
      </div>
    </div>
  )
}

export default OverviewCardRed